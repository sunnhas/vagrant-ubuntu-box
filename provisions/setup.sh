echo "Starting the provision"

# Update the system
#sudo apt-get update

# Setup danish keyboard
sudo loadkeys dk

# Install the gui
sudo apt-get install -y xfce4 #virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
sudo sed -i 's/allowed_users=.*$/allowed_users=anybody/' /etc/X11/Xwrapper.config

echo "Done with the provision"
